﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Project1.Data;
using Project1.Models;

namespace Project1.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ProductsController(WarehouseContext context) : ControllerBase
    {
        private readonly WarehouseContext _context = context;

        [HttpGet("/search-products/{page}")]
        public async Task<ActionResult<string>> GetProducts(int page = 1)
        {
            var products = await _context.Products.Skip((page - 1) * 10).Take(10).ToListAsync();
            if (products == null || !products.Any()) return NotFound("No products found."); return Ok(new { products });
        }

        [HttpGet("/search-products-by-tags/{page}/{tags}")]
        public async Task<ActionResult<string>> SearchByTags(string tags, int page = 1)
        {
            if (string.IsNullOrWhiteSpace(tags)) return BadRequest("Tags parameter is required."); 
            var tagList = tags.Split(',');
            var products = await _context.Products.Where(p => tagList.Intersect(p.Tags).Count() == tagList.Length).Skip((page - 1) * 10).Take(10).ToListAsync();
            if (products == null || !products.Any()) return NotFound("No products found with the specified tags."); return Ok(new { products });
        }

        [HttpPost("/add-products"), Authorize]
        public async Task<ActionResult<string>> PostProducts(List<Product> products)
        {
            if (products == null || products.Count == 0) return BadRequest("No products provided for bulk insertion.");
            _context.Products.AddRange(products); await _context.SaveChangesAsync(); return Ok(new { products });
        }








        // GET: api/Products/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Product>> GetProduct(int id)
        {
            var product = await _context.Products.FindAsync(id);

            if (product == null)
            {
                return NotFound();
            }

            return product;
        }

        // PUT: api/Products/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProduct(int id, Product product)
        {
            if (id != product.Id)
            {
                return BadRequest();
            }

            _context.Entry(product).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }



        // DELETE: api/Products/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduct(int id)
        {
            var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            _context.Products.Remove(product);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ProductExists(int id)
        {
            return _context.Products.Any(e => e.Id == id);
        }
    }
}
