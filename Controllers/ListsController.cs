﻿using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using Project1.Data;
using Project1.Models;
using Project1.Services;

namespace Project1.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ListsController(WarehouseContext context, ListService listService) : ControllerBase
    {
        private readonly WarehouseContext _context = context;
        private readonly ListService _listService = listService;

        [HttpGet("get-user-list"), Authorize]
        public async Task<ActionResult<string>> GetUserList()
        {
            var userIdClaim = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier);
            if (userIdClaim == null) Unauthorized("User not found in the JWT.");
            var userList = await _listService.GetList(userIdClaim.Value);
            var listItems = await _listService.GetListItems((int)userList.Id); 
            return Ok(new { listItems });
        }

        [HttpPost("add-list-items"), Authorize]
        public async Task<ActionResult<string>> PostAddListItems(ListItem newListItem)
        {
            var userIdClaim = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier);
            if (userIdClaim == null) Unauthorized("User not found in the JWT.");
            var userList = await _listService.GetList(userIdClaim.Value);
            var listItems = await _listService.AddListItems(newListItem, userList);
            return Ok(new { listItems });
        }

        [HttpDelete("reduce-list-items"), Authorize]
        public async Task<ActionResult<string>> ReduceListItems(ListItem newListItem)
        {
            var userIdClaim = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier);
            if (userIdClaim == null) Unauthorized("User not found in the JWT.");
            var userList = await _listService.GetList(userIdClaim.Value);
            var listItems = await _listService.ReduceListItems(newListItem, userList);
            return Ok(new { listItems });
        }

        [HttpDelete("remove-list-items"), Authorize]
        public async Task<ActionResult<string>> RemoveListItems(ListItem newListItem)
        {
            var userIdClaim = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier);
            if (userIdClaim == null) Unauthorized("User not found in the JWT.");
            var userList = await _listService.GetList(userIdClaim.Value);
            var listItems = await _listService.RemoveListItems(newListItem, userList);
            return Ok(new { listItems });
        }
    }
}
