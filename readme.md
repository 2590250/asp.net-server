Server for storage database that can be used with a desktop application with asp.net core, entity framework and identity framework, there is service files and global error handling
Users can add items to a list and make orders from the storage

Currently implemented features:
- Get list items of the user
- Add new items to the list, if the user adds an item which already exists, it will increase the quantity of the existing item
- Reduce quantity of list items, product will be automatically removed if quantity is zero or lower
- Delete list items
- Get all items of storage
- Get all items of storage filtered by multiple tags
- Add items to the items table in bulks
- Pagination listing
