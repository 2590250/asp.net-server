﻿namespace Project1.Models
{
    public class Order
    {
        public int? Id { get; set; }
        public string? UserId { get; set; }
        public int? Total { get; set; }
        public string? State { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }
    }
}
