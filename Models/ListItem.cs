﻿
namespace Project1.Models
{
    public class ListItem
    {
        public int? Id { get; set; }
        public int? ListId { get; set; }
        public int? ProductId { get; set; }
        public int? Quantity { get; set; }

    }
}
