﻿using System.Text.Json;

namespace Project1.Models
{
    public class ErrorDetails
    {
        public int ResponseCode { get; set; }
        public string? Message { get; set; }
        public override string ToString() => JsonSerializer.Serialize(this);
    }
}
