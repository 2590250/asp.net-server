﻿namespace Project1.Models
{
    public class List
    {
        public int? Id { get; set; }
        public string? UserId { get; set; }
        public DateTime? Modified { get; set; }
        public DateTime? Created { get; set; }
        public bool? Converted { get; set; }
        public bool? IsActive { get; set; }
    }
}
