﻿
namespace Project1.Models
{
    public class ListItemProductJoin
    {
        public int? ListItemId { get; set; }
        public int? ListItemListId { get; set; }
        public int? ListItemProductId { get; set; }
        public int? ListItemQuantity { get; set; }
        public string? ProductName { get; set; }
        public int? ProductPrice { get; set; }
    }
}
