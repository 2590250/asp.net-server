﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Project1.Data;
using Project1.Models;

namespace Project1.Services
{
    public class ListService(WarehouseContext context)
    {
        private readonly WarehouseContext _context = context;

        public async Task<List> GetList(string userId)
        {
            var userList = await _context.Lists.FirstOrDefaultAsync(c => c.UserId == userId);

            if (userList == null)
            {
                userList = new List
                {
                    UserId = userId,
                    Modified = DateTime.Now,
                    Created = DateTime.Now,
                    Converted = false,
                    IsActive = true
                };

                _context.Lists.Add(userList);
                await _context.SaveChangesAsync();

                userList = await _context.Lists.FirstOrDefaultAsync(c => c.UserId == userId);
            }

            return userList;
        }

        public async Task<List<ListItemProductJoin>> GetListItems(int listId)
        {
            var innerJoinQuery = from listItem in _context.ListItems
                                 join product in _context.Products on listItem.ProductId equals product.Id
                                 where listItem.ListId == listId
                                 select new ListItemProductJoin
                                 {
                                     ListItemId = listItem.Id,
                                     ListItemListId = listItem.ListId,
                                     ListItemProductId = listItem.ProductId,
                                     ListItemQuantity = listItem.Quantity,
                                     ProductName = product.Name,
                                     ProductPrice = product.Price,
                                 };

            return await innerJoinQuery.ToListAsync();
        }

        public async Task<ListItem> AddListItems(ListItem newListItem, List userList)
        {

            newListItem.ListId = userList.Id;

            var existingListItem = await _context.ListItems
                .FirstOrDefaultAsync(ci => ci.ListId == newListItem.ListId && ci.ProductId == newListItem.ProductId);

            if (existingListItem != null)
            {
                _context.ListItems.Remove(existingListItem);

                existingListItem = new ListItem
                {
                    ListId = existingListItem.ListId,
                    ProductId = existingListItem.ProductId,
                    Quantity = existingListItem.Quantity + newListItem.Quantity
                };

                _context.ListItems.Add(existingListItem);
                await _context.SaveChangesAsync();
                return existingListItem;
            }
            else
            {
                _context.ListItems.Add(newListItem);
                await _context.SaveChangesAsync();
                return newListItem;
            }
        }

        public async Task<ListItem> ReduceListItems(ListItem newListItem, List userList)
        {

            newListItem.ListId = userList.Id;

            var existingListItem = await _context.ListItems
                .FirstOrDefaultAsync(ci => ci.ListId == newListItem.ListId && ci.ProductId == newListItem.ProductId);

            if (existingListItem != null)
            {
                _context.ListItems.Remove(existingListItem);

                existingListItem = new ListItem
                {
                    ListId = existingListItem.ListId,
                    ProductId = existingListItem.ProductId,
                    Quantity = existingListItem.Quantity - newListItem.Quantity
                };

                if (existingListItem.Quantity < 0)
                {
                    await _context.SaveChangesAsync();
                    return existingListItem; 
                }

                _context.ListItems.Add(existingListItem);
                await _context.SaveChangesAsync();
                return existingListItem;

            }
            else
            {
                throw new InvalidOperationException("The specified list item was not found.");
            }
        }

        public async Task<ListItem> RemoveListItems(ListItem newListItem, List userList)
        {

            newListItem.ListId = userList.Id;

            var existingListItem = await _context.ListItems
                .FirstOrDefaultAsync(ci => ci.ListId == newListItem.ListId && ci.ProductId == newListItem.ProductId);

            if (existingListItem != null)
            {
                _context.ListItems.Remove(existingListItem);
                await _context.SaveChangesAsync();
                return existingListItem;
            }
            else
            {
                throw new InvalidOperationException("The specified list item was not found.");
            }
        }
    }
}
